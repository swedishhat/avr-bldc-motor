/* 
ATtiny85 motor controller
Uses ATtiny85 and 8-bit shift register for
open-loop motor control and LED output

Bit combinations
Ch Cl Bh Bl Ah Al
 0  0  0  1  1  0 = 0x06
 1  0  0  1  0  0 = 0x24
 1  0  0  0  0  1 = 0x21
 0  0  1  0  0  1 = 0x09
 0  1  1  0  0  0 = 0x18
 0  1  0  0  1  0 = 0x12

TODO
Add closed loop functionality

*/

#define F_CPU 1000000UL

#include <avr/io.h>
#include <util/delay.h>

#define DATA0   1
#define DATA1   2
#define CLK0    3
#define CLK1    4
#define STR     5

void print7seg(int);    // Accepts integer 9..0 and displays it on the 7 segment LCD
void rundatrack(int);   // Accepts integer 6..1 and runs the motor
void shiftin(char[], int);      // Accepts an integer and toggles the CLK pin accordingly
void error4ever(void);

int main(void)
{
    DDRB =  0x1F;    // 5 Outputs: 2x Data, 2x Clock, Strobe
    PORTB = 0x00;    // Set all bits low
    
    while(1)
    {
        for(int i = 1; i <= 6; i++)
        {
            rundatrack(i);
            print7seg(i);
            _delay_ms(1000);
        }
    }
}

/*
Prints single digit integer to a 7-segment display.
Segments: a, b, c, d, e, f, g, dp
*/
void print7seg(int n)
{ 
    switch(n)
    {
        case 0:
            // abcdef = 1111 1100 = 0xFC
            shiftin("11111100", 1);
            break;
        case 1:
            // bc = 0110 0000
            shiftin("01100000", 1);
            break;
        case 2:
            // abdeg = 1101 1010
            shiftin("11011010", 1);
            break;
        case 3:
            // abcdg = 1111 0010
            shiftin("11110010", 1);
            break;
        case 4:
            // bcfg = 0110 0110 
            shiftin("01000110", 1);
            break;
        case 5:
            // acdfg = 1011 0110
            shiftin("10110110", 1);
            break;
        case 6:
            // cdefg = 0011 1110
            shiftin("00111110", 1);
            break;
        case 7:
            // abc = 1110 0000
            shiftin("11100000", 1);
            break;
        case 8:
            // abcdefg = 1111 1110
            shiftin("11111110", 1);
            break;
        case 9:
            // abcfg = 1110 0110
            shiftin("11100110", 1);
            break;
        default:
            // Error condition: prints "E."
            // adefg. = 10011111
            shiftin("10011111", 1);
            break;
    }    
    // Strobe it!
    PORTB |= 1 << STR;
}

// Permanent failure mode. Requires reset
void error4ever(){
    print7seg(10);
    while(1){}
}

/*
Accepts a char string with 8 "bits" as well as the register
and loads the values into the corresponding register. 
*/
void shiftin(char val[], int reg)
{
    int datapin = 0;
    int clkpin = 0;
    
    switch(reg)
    {
        case 0:
            datapin = DATA0;
            clkpin = CLK0;
            break;
        case 1:
            datapin = DATA1;
            clkpin = CLK1;
            break;
         default:
            error4ever();
            break;
    }

    // I'm assuming that I will only get 8 bits in here. 
    for(int i = 0; i <= 7; i++)
    {
        PORTB |= val[i] << datapin;
        PORTB |= 1 << clkpin;
        //_delay_ms(1);     // IDK if the compiler will do something funny here. Delay if necessary
        PORTB |= 0 << clkpin;
    }
    
    // Clear the port!
    PORTB = 0x00;
}

void rundatrack(int state)
{
    switch(state)
    {
        case 1:
            // 6
            shiftin("01001000", 0);
            break;
        case 2:
            // 4
            shiftin("00100100", 0);
            break;
        case 3:
            // 5
            shiftin("01100000", 0);
            break;
        case 4:
            // 2
            shiftin("10010000", 0);
            break;
        case 5:
            // 1
            shiftin("00010000", 0);
            break;
        case 6:
            // 3
            shiftin("10000100", 0);
            break;
        default:
            error4ever();
            break;
    }
    // STROBE IT!!!!
    PORTB |= 1 << STR;
}
